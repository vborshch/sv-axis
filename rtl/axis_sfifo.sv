/*
  parameter int DATA_WIDTH  =   8;
  parameter int DEPTH       = 256;
  parameter int FRAME       =   1;
  parameter int USER_ENA    =   1;
  parameter int LAST_ENA    =   1;

  logic                  sfifo__iclk          ;
  logic                  sfifo__irst          ;
  logic [DATA_WIDTH-1:0] sfifo__s_axis_tdata  ;
  logic                  sfifo__s_axis_tvalid ;
  logic                  sfifo__s_axis_tlast  ;
  logic                  sfifo__s_axis_tready ;
  logic                  sfifo__s_axis_tuser  ;
  logic [DATA_WIDTH-1:0] sfifo__m_axis_tdata  ;
  logic                  sfifo__m_axis_tready ;
  logic                  sfifo__m_axis_tvalid ;
  logic                  sfifo__m_axis_tlast  ;
  logic                  sfifo__m_axis_tuser  ;
  logic                  sfifo__empty         ;
  logic                  sfifo__full          ;
  logic                  sfifo__overflow      ;
  logic                  sfifo__underflow     ;

  axis_sfifo
  #(
    . DATA_WIDTH    (DATA_WIDTH      ) ,
    . DEPTH         (DEPTH           ) ,
    . FRAME         (FRAME           ) ,
    . USER_ENA      (USER_ENA        ) ,
    . LAST_ENA      (LAST_ENA        )
  )
  sfifo__
  (
    .iclk          (sfifo__iclk               ) ,
    .irst          (sfifo__irst               ) ,
    .s_axis_tdata  (sfifo__s_axis_tdata       ) ,
    .s_axis_tvalid (sfifo__s_axis_tvalid      ) ,
    .s_axis_tlast  (sfifo__s_axis_tlast       ) ,
    .s_axis_tready (sfifo__s_axis_tready      ) ,
    .s_axis_tuser  (sfifo__s_axis_tuser       ) ,
    .m_axis_tdata  (sfifo__m_axis_tdata       ) ,
    .m_axis_tready (sfifo__m_axis_tready      ) ,
    .m_axis_tvalid (sfifo__m_axis_tvalid      ) ,
    .m_axis_tlast  (sfifo__m_axis_tlast       ) ,
    .m_axis_tuser  (sfifo__m_axis_tuser       ) ,
    .empty         (sfifo__empty              ) ,
    .full          (sfifo__full               ) ,
    .overflow      (sfifo__overflow           ) ,
    .underflow     (sfifo__underflow          ) 
  );

  assign sfifo__iclk          = '0;
  assign sfifo__irst          = '0;
  assign sfifo__s_axis_tdata  = '0;
  assign sfifo__s_axis_tvalid = '0;
  assign sfifo__s_axis_tlast  = '0;
  assign sfifo__m_axis_tready = '0;
*/

module axis_sfifo
#(
  parameter int DATA_WIDTH =   8 ,
  parameter int DEPTH      = 256 ,
  parameter int FRAME      =   1 ,
  parameter int USER_ENA   =   1 ,
  parameter int USER_WIDTH =   8 ,
  parameter int LAST_ENA   =   1
)
(
  input  logic                      iclk          ,
  input  logic                      irst          ,
  input  logic     [DATA_WIDTH-1:0] s_axis_tdata  ,
  input  logic                      s_axis_tvalid ,
  input  logic                      s_axis_tlast  ,
  input  logic     [USER_WIDTH-1:0] s_axis_tuser  ,
  output logic                      s_axis_tready ,
  output logic     [DATA_WIDTH-1:0] m_axis_tdata  ,
  input  logic                      m_axis_tready ,
  output logic                      m_axis_tvalid ,
  output logic     [USER_WIDTH-1:0] m_axis_tuser  ,
  output logic                      m_axis_tlast  ,
  //
  output logic  [$clog2(DEPTH)-1:0] used          ,
  output logic                      empty         ,
  output logic                      full          ,
  output logic                      overflow      = '0,
  output logic                      underflow     = '0
);

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  localparam int ADDR_W = $clog2(DEPTH);
  localparam int USER_W = (USER_ENA) ? (DATA_WIDTH+USER_WIDTH) : (DATA_WIDTH);
  localparam int LAST_W = (LAST_ENA) ? (USER_W             +1) : (USER_W);
  localparam int WIDTH  = LAST_W;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  logic     [WIDTH-1:0] mem [DEPTH];
  logic      [ADDR_W:0] wr_ptr     = '0;
  logic      [ADDR_W:0] wr_ptr_cur = '0;
  logic      [ADDR_W:0] rd_ptr     = '0;
  logic                 drop       = '0;
  logic                 tvalid     = '0;
  logic     [WIDTH-1:0] tdata      = '0;
  logic     [WIDTH-1:0] s_axis     ;
  logic                 full_cur   ;
  logic                 full_wr    ;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  // Full when first MSB different but rest the same
  assign full = (wr_ptr == (rd_ptr ^ {1'b1, {ADDR_W{1'b0}}}));
  assign full_cur = (wr_ptr_cur == (rd_ptr ^ {1'b1, {ADDR_W{1'b0}}}));
  // Empty when pointers match exactly
  assign empty = (wr_ptr == rd_ptr);
  // Overflow within packet
  assign full_wr = (wr_ptr == (wr_ptr_cur ^ {1'b1, {ADDR_W{1'b0}}}));
  assign used = (full && ~overflow) ? ('1) : (wr_ptr - rd_ptr);

generate
                assign s_axis[DATA_WIDTH-1:0]       = s_axis_tdata;
  if (LAST_ENA) assign s_axis[DATA_WIDTH]           = s_axis_tlast;
  if (USER_ENA) assign s_axis[WIDTH-1:DATA_WIDTH+1] = s_axis_tuser;
endgenerate

  // Writing
  always_ff@(posedge iclk) begin
    if (irst) begin
      wr_ptr <= '0;
      wr_ptr_cur <= '0;
      drop <= '0;
    end else if (s_axis_tvalid && s_axis_tready) begin

      if (!FRAME) begin
        mem[wr_ptr[ADDR_W-1:0]] <= s_axis;
        wr_ptr <= wr_ptr + 1'b1;
      end else if (full_wr || full_cur || drop) begin
        drop <= 1'b1;

        if (s_axis_tlast) begin
          wr_ptr_cur <= wr_ptr;
          overflow <= 1'b1;
          drop <= 1'b0;
        end
      end else begin
        mem[wr_ptr_cur[ADDR_W-1:0]] <= s_axis;
        wr_ptr_cur <= wr_ptr_cur + 1'b1;

        if (s_axis_tlast) begin
          wr_ptr <= wr_ptr_cur + 1'b1;
        end
      end

    end
  end

  // Reading
  always_ff@(posedge iclk) begin
    if (irst) begin
      rd_ptr <= '0;
    end else if (m_axis_tready || ~tvalid) begin
      tdata <= mem[rd_ptr[ADDR_W-1:0]];

      // Not empty, increment pointer
      if (!empty) begin
        rd_ptr <= rd_ptr + 1'b1;
      end
    end

    if (irst) begin
      tvalid <= 1'b0;
    end else if (!empty) begin
      tvalid <= 1'b1;
    end else if (m_axis_tready) begin
      tvalid <= 1'b0;
    end
  end

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  assign s_axis_tready = (FRAME) ? (!full_cur || full_wr) : (!full);
  assign m_axis_tvalid = tvalid;
  assign m_axis_tdata  =               tdata[DATA_WIDTH-1:            0];
  assign m_axis_tlast  = (LAST_ENA) ? (tdata[DATA_WIDTH               ]) : (1'b1);
generate
  if (USER_ENA)
    assign m_axis_tuser = tdata[WIDTH-1:DATA_WIDTH+1];
  else
    assign m_axis_tuser = 1'bx;
endgenerate

endmodule : axis_sfifo




// synthesis translate_off
module axis_fifo_sc_tb;
  parameter int DATA_WIDTH =   8;
  parameter int DEPTH      = 256;
  parameter int FRAME      =   1;
  parameter int FWFT       =   1;

  logic                  iclk          = '0;
  logic                  irst          = '0;
  logic [DATA_WIDTH-1:0] s_axis_tdata  = '0;
  logic                  s_axis_tvalid = '0;
  logic                  s_axis_tlast  = '0;
  logic                  s_axis_tready ;
  logic [DATA_WIDTH-1:0] m_axis_tdata  ;
  logic                  m_axis_tready ;
  logic                  m_axis_tvalid ;
  logic                  m_axis_tlast  ;
  logic                  empty         ;
  logic                  full          ;
  logic                  overflow      ;
  logic                  underflow     ;

  logic                  mready        = 1'b0;

  initial forever begin
    #1;
    iclk = !iclk;
  end

  initial begin
    irst = 1'b1; repeat(4)@(posedge iclk); irst = 1'b0;
    //
    repeat(32) begin
      repeat(32)@(posedge iclk) begin
        s_axis_tvalid = 1'b1;
        s_axis_tdata = $random();
      end
      s_axis_tdata = $random();
      s_axis_tlast = 1'b1;
      @(posedge iclk)
      s_axis_tvalid = 1'b0;
      s_axis_tlast = 1'b0;

      repeat(12)@(posedge iclk);
    end
  end

  initial begin
    repeat(256)@(posedge iclk);
    mready = 1'b1;
    repeat(256)@(posedge iclk) begin
      if (m_axis_tlast) begin
        mready = 1'b0;
        repeat(4)@(posedge iclk);
      end
      mready = 1'b1;
    end
  end

  assign m_axis_tready = mready & ~m_axis_tlast;

  axis_fifo_sc
  #(
    . DATA_WIDTH    (DATA_WIDTH      ) ,
    . DEPTH         (DEPTH           ) ,
    . FRAME         (FRAME           ) ,
    . FWFT          (FWFT            ) 
  )
  axis_fifo_sc__
  (
    .*
  );

endmodule : axis_fifo_sc_tb
// synthesis translate_on
