# SystemVerilog AXI stream modules Readme

[![pipeline status](https://gitlab.com/vborshch/sv-axis/badges/master/pipeline.svg)](https://gitlab.com/vborshch/sv-axis/-/commits/master)

## Introduction

This is a bunch of AXI stream common used modules.

Part of this modules (ex. COBS) are totally stilled from [Alex Forencich](https://gitlab.com/alex.forencich). Others are original.

The test environment is bulded with cocotb.

## Dependencies

- Python 3.8
- Icarus Verilog 12.0 *or* ModelSim/QuestaSim
- cocotb 1.6.1
- SystemVerilog IEEE 1800-2012

> Feel free to use any simulator with SV support

## Modules list

TODO
