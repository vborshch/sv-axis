#!/usr/bin/env python

import itertools
import logging
import random
import numpy as np
from scipy import signal

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge
from cocotb.regression import TestFactory
from cocotbext.axi import AxiStreamBus, AxiStreamFrame, AxiStreamSource, AxiStreamSink


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class TB(object):
    def __init__(self, dut):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        cocotb.fork(Clock(dut.iclk, 1, units="ns").start())

        self.source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis"), dut.iclk, dut.irst)
        # self.sink = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis"), dut.iclk, dut.irst)

    async def reset(self):
        self.dut.irst.setimmediatevalue(0)
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst.value = 1
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst.value = 0
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test(
    dut,
    payload_lengths=None,
    payload_data=None,
    idle_inserter=None,
    backpressure_inserter=None,
    n_aver=2
):
    tb = TB(dut)

    rtl_bitwidth = int(tb.dut.W)

    tb.dut.iavr.value = 0
    tb.dut.ibypass.value = 0

    await tb.reset()

    tb.dut.iavr.value = n_aver

    tx_frames = [payload_data(x) for x in payload_lengths()]
    rx_frames = []

    for tx_frame in tx_frames:
        print(tx_frame)
        tx_frame = [int(x * 2**(rtl_bitwidth-2)) for x in tx_frame]

        print(tx_frame)

        byte_data = b''
        for x in tx_frame:
            byte_data += x.to_bytes(length=2, byteorder="little", signed=True)

        await tb.source.send(AxiStreamFrame(byte_data))

    # for tx_frame in tx_frames:
    #     rx_frame = await tb.sink.recv()

    for _ in range(256):
        await RisingEdge(dut.iclk)

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_incrementing(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_constant(length):
    return bytearray(itertools.islice(itertools.cycle([111]), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_sin_noise(length):
    cycles = 2 # how many sine cycles
    resolution = 128 # how many datapoints to generate
    length = np.pi * 2 * cycles
    wave = np.sin(np.arange(0, length, length / resolution))
    noise = np.random.normal(0, 0.005, len(wave))
    wave = np.add(wave, noise)
    wave = wave/max(wave)
    return wave

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_square(length):
    t = np.linspace(0, 1, length, endpoint=False)
    square = signal.square(2 * np.pi * 5 * t)
    return square


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    return list(range(253, 259))+[512]


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def cycle_pause():
    return itertools.cycle([1, 1, 1, 0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    factory = TestFactory(run_test)
    factory.add_option("payload_lengths", [size_list])
    factory.add_option("n_aver", [16, 4, 2])
    factory.add_option("payload_data", [payload_square, payload_sin_noise])
    factory.generate_tests()
