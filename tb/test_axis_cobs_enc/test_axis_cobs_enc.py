#!/usr/bin/env python

import itertools
import logging
import os
import random

import cocotb_test.simulator
import pytest

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge
from cocotb.regression import TestFactory
from cocotbext.axi import AxiStreamBus, AxiStreamFrame, AxiStreamSource, AxiStreamSink

from cobs_misc import *


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class TB(object):
    def __init__(self, dut):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        cocotb.fork(Clock(dut.iclk, 1, units="ns").start())

        self.source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis"), dut.iclk, dut.irst)
        self.sink = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis"), dut.iclk, dut.irst)
    
    def set_idle_generator(self, generator=None):
        if generator:
            self.source.set_pause_generator(generator())

    def set_backpressure_generator(self, generator=None):
        if generator:
            self.sink.set_pause_generator(generator())

    async def reset(self):
        self.dut.irst.setimmediatevalue(0)
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst <= 1
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst <= 0
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = TB(dut)
    append_zero = int(os.getenv("PARAM_ZERO_APPEND"))

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    tx_frames = [payload_data(x) for x in payload_lengths()]
    rx_frames = []

    for tx_frame in tx_frames:
        await tb.source.send(AxiStreamFrame(tx_frame))

    for tx_frame in tx_frames:
        rx_frame = await tb.sink.recv()

        assert rx_frame.tdata == cobs_encode(tx_frame)
        assert cobs_decode(rx_frame.tdata) == tx_frame

    assert tb.sink.empty()

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_zeros(length):
    return bytearray(length)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_incrementing(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_incrementing_nonzero(length):
    return bytearray(itertools.islice(itertools.cycle(range(1, 256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_incrementing_zeroframing(length):
    return bytearray([0]+list(itertools.islice(itertools.cycle(range(1, 256)), length))+[0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs31(state=0x7fffffff):
    while True:
        for i in range(8):
            if bool(state & 0x08000000) ^ bool(state & 0x40000000):
                state = ((state & 0x3fffffff) << 1) | 1
            else:
                state = (state & 0x3fffffff) << 1
        yield state & 0xff


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def payload_prbs(length):
    gen = prbs31()
    return bytearray([next(gen) for x in range(length)])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    return list(range(1, 33))+list(range(253, 259))+[512]+[1]*64


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def cycle_pause():
    return itertools.cycle([1, 1, 1, 0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    factory = TestFactory(run_test)
    factory.add_option("payload_lengths", [size_list])
    factory.add_option("payload_data", [payload_zeros, payload_incrementing, payload_incrementing_nonzero, payload_incrementing_zeroframing, payload_prbs])
    factory.add_option("idle_inserter", [None, cycle_pause])
    factory.add_option("backpressure_inserter", [None, cycle_pause])
    factory.generate_tests()
