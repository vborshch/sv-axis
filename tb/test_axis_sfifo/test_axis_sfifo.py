#!/usr/bin/env python

import itertools
import logging
import os
import random

import cocotb_test.simulator
import pytest

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge
from cocotb.regression import TestFactory
from cocotbext.axi import AxiStreamBus, AxiStreamFrame, AxiStreamSource, AxiStreamSink


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class tb_axis_sfifo(object):
    def __init__(self, dut):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        cocotb.fork(Clock(dut.iclk, 1, units="ns").start())

        self.source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis"), dut.iclk, dut.irst)
        self.sink = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis"), dut.iclk, dut.irst)
    
    def set_idle_generator(self, generator=None):
        if generator:
            self.source.set_pause_generator(generator())

    def set_backpressure_generator(self, generator=None):
        if generator:
            self.sink.set_pause_generator(generator())

    async def reset(self):
        self.dut.irst.setimmediatevalue(1)
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst <= 0
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = tb_axis_sfifo(dut)
    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    tx_frames = []
    rx_frames = []

    for test_data in [payload_data(x) for x in payload_lengths()]:
        test_frame = AxiStreamFrame(test_data)
        test_frame.tuser = test_data[-1]
        tx_frames.append(test_frame)
        await tb.source.send(test_frame)

    for frame in tx_frames:
        rx_frame = await tb.sink.recv()
        assert rx_frame.tdata == frame.tdata
        assert rx_frame.tuser == frame.tuser

    assert tb.sink.empty()

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_init_sink_pause(dut):
    tb = tb_axis_sfifo(dut)
    await tb.reset()

    tb.sink.pause = True

    test_data = bytearray(itertools.islice(itertools.cycle(range(256)), 32))
    test_frame = AxiStreamFrame(test_data)
    test_frame.tuser = test_data[-1]
    await tb.source.send(test_frame)

    for k in range(64):
        await RisingEdge(dut.iclk)

    tb.sink.pause = False

    rx_frame = await tb.sink.recv()
    assert rx_frame.tdata == test_frame.tdata
    assert rx_frame.tuser == test_frame.tuser

    assert tb.sink.empty()

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_init_sink_pause_reset(dut):
    tb = tb_axis_sfifo(dut)
    await tb.reset()

    tb.sink.pause = True

    test_data = bytearray(itertools.islice(itertools.cycle(range(256)), 32))
    test_frame = AxiStreamFrame(test_data)
    test_frame.tuser = test_data[-1]
    await tb.source.send(test_frame)

    for k in range(64):
        await RisingEdge(dut.iclk)

    await tb.reset()

    tb.sink.pause = False

    for k in range(64):
        await RisingEdge(dut.iclk)

    assert tb.sink.empty()

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_overflow(dut):
    tb = tb_axis_sfifo(dut)
    await tb.reset()

    tb.sink.pause = True

    test_data = bytearray(itertools.islice(itertools.cycle(range(256)), 2048))
    test_frame = AxiStreamFrame(test_data)
    test_frame.tuser = test_data[-1]
    await tb.source.send(test_frame)

    for k in range(2048):
        await RisingEdge(dut.iclk)

    tb.sink.pause = False

    rx_frame = await tb.sink.recv()

    assert rx_frame.tdata == test_frame.tdata
    assert rx_frame.tuser == test_frame.tuser

    assert tb.sink.empty()

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_stress_test(dut, idle_inserter=None, backpressure_inserter=None):
    tb = tb_axis_sfifo(dut)

    byte_lanes = tb.source.byte_lanes

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    test_frames = []

    for k in range(128):
        length = random.randint(1, byte_lanes*16)
        test_data = bytearray(itertools.islice(itertools.cycle(range(256)), length))
        test_frame = AxiStreamFrame(test_data)
        test_frame.tuser = test_data[-1]

        test_frames.append(test_frame)
        await tb.source.send(test_frame)

    for test_frame in test_frames:
        rx_frame = await tb.sink.recv()

        assert rx_frame.tdata == test_frame.tdata
        assert rx_frame.tuser == test_frame.tuser

    assert tb.sink.empty()

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    data_width = len(cocotb.top.s_axis_tdata)
    byte_width = data_width // 8
    return list(range(1, byte_width*8+1))+[128]+[1]*64


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def cycle_pause():
    return itertools.cycle([1, 1, 1, 0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    factory = TestFactory(run_test)
    factory.add_option("payload_lengths", [size_list])
    factory.add_option("payload_data", [incrementing_payload])
    factory.add_option("idle_inserter", [None, cycle_pause])
    factory.add_option("backpressure_inserter", [None, cycle_pause])
    factory.generate_tests()

    ## for test in [run_test_init_sink_pause, run_test_init_sink_pause_reset, run_test_overflow]:
    for test in [run_test_init_sink_pause, run_test_init_sink_pause_reset]:
        factory = TestFactory(test)
        factory.generate_tests()

    factory = TestFactory(run_stress_test)
    factory.add_option("idle_inserter", [None, cycle_pause])
    factory.add_option("backpressure_inserter", [None, cycle_pause])
    factory.generate_tests()
