/*
  parameter int W   = 14;
  parameter int W_A =  4;

  logic            aver__irst         ;
  logic            aver__iclk         ;
  logic            aver__ibypass      ;
  logic            aver__s_axis_tvalid;
  logic    [W-1:0] aver__s_axis_tdata ;
  logic            aver__m_axis_tvalid;
  logic    [W-1:0] aver__m_axis_tdata ;
  logic  [W_A-1:0] aver__iavr         ;

  axis_mov_aver
  #(
    . W      (W        ) ,
    . W_A    (W_A      ) 
  )
  mov_aver
  (
    .irst          (aver__irst           ) ,
    .iclk          (aver__iclk           ) ,
    .ibypass       (aver__ibypass        ) ,
    .s_axis_tvalid (aver__s_axis_tvalid  ) ,
    .s_axis_tdata  (aver__s_axis_tdata   ) ,
    .m_axis_tvalid (aver__m_axis_tvalid  ) ,
    .m_axis_tdata  (aver__m_axis_tdata   ) ,
    .iavr          (aver__iavr           )
  );

  assign aver__irst          = '0;
  assign aver__iclk          = '0;
  assign aver__ibypass       = '0;
  assign aver__s_axis_tvalid = '0;
  assign aver__s_axis_tdata  = '0;
  assign aver__iavr          = '0;
*/ 

module axis_mov_aver
#(
  parameter int W   = 14 ,
  parameter int W_A =  5
)
(
  input  logic            irst          ,
  input  logic            iclk          ,
  input  logic            s_axis_tvalid ,
  input  logic    [W-1:0] s_axis_tdata  ,
  output logic            m_axis_tvalid ,
  output logic    [W-1:0] m_axis_tdata  ,
  input  logic            ibypass       ,
  input  logic  [W_A-1:0] iavr
);

  localparam int W_ACC = W+W_A;

  //------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------

  logic          [W-1:0] buf_delay [2**W_A];
  logic        [W_A-1:0] f_iavr;
  logic                  navr_upd;
  logic                  merge_reset;
  logic        [W_A-1:0] wr_ptr;
  logic        [W_A-1:0] rd_ptr;
  logic          [W-1:0] sample;
  logic                  buf_rdy;
  logic      [W_ACC-1:0] acc;
  logic            [1:0] tvalid;

  //------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------

  // Detect coefficient changing
  always_ff@(posedge iclk) begin
    if (irst) begin
      f_iavr <= '0;
      navr_upd <= '0;
    end else begin
      f_iavr <= iavr;
      navr_upd <= f_iavr != iavr;
    end
  end

  assign merge_reset = navr_upd | irst;

  always_ff@(posedge iclk) begin
    if (merge_reset) begin
      wr_ptr <= iavr-1;
      rd_ptr <= '0;
      buf_rdy <= 1'b0;
    end else if (s_axis_tvalid) begin
      wr_ptr <= wr_ptr + 1'b1;
      rd_ptr <= rd_ptr + 1'b1;

      // Delay by 2**W_A samples maximum
      buf_delay[wr_ptr] <= s_axis_tdata;
      sample <= buf_delay[rd_ptr];

      if (rd_ptr == iavr-1)
        buf_rdy <= 1'b1;
    end
  end

  // Integrator
  always_ff@(posedge iclk) begin
    // Rise after 2 clock cycles of module's pipeline
    tvalid <= tvalid << 1 | s_axis_tvalid;

    if (merge_reset)                    acc <= '0;
    else if (buf_rdy && s_axis_tvalid)  acc <= $signed(acc) + $signed(s_axis_tdata) - $signed(sample);
      else if (s_axis_tvalid)           acc <= $signed(acc) + $signed(s_axis_tdata);
  end

  //------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------

  always_ff @(posedge iclk) begin
    if (merge_reset) begin
      m_axis_tvalid <= '0;
      m_axis_tdata <= '0;
    end else if (ibypass) begin
      m_axis_tvalid <= s_axis_tvalid;
      m_axis_tdata <= s_axis_tdata;
    end else begin
      m_axis_tvalid <= tvalid[$high(tvalid)];
      m_axis_tdata <= $signed(acc)/iavr;
    end
  end

endmodule
