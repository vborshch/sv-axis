/*
  parameter int S_DATA_WIDTH = 8;
  parameter int M_DATA_WIDTH = 8;

  logic                    reducer__iclk          ;
  logic                    reducer__irst          ;
  logic [S_DATA_WIDTH-1:0] reducer__s_axis_tdata  ;
  logic                    reducer__s_axis_tvalid ;
  logic                    reducer__s_axis_tlast  ;
  logic                    reducer__s_axis_tready ;
  logic [M_DATA_WIDTH-1:0] reducer__m_axis_tdata  ;
  logic                    reducer__m_axis_tready ;
  logic                    reducer__m_axis_tvalid ;
  logic                    reducer__m_axis_tlast  ;

  axis_reducer
  #(
    . S_DATA_WIDTH    (S_DATA_WIDTH      ) ,
    . M_DATA_WIDTH    (M_DATA_WIDTH      ) 
  )
  axis_reducer
  (
    .iclk          (reducer__iclk               ) ,
    .irst          (reducer__irst               ) ,
    .s_axis_tdata  (reducer__s_axis_tdata       ) ,
    .s_axis_tvalid (reducer__s_axis_tvalid      ) ,
    .s_axis_tlast  (reducer__s_axis_tlast       ) ,
    .s_axis_tready (reducer__s_axis_tready      ) ,
    .m_axis_tdata  (reducer__m_axis_tdata       ) ,
    .m_axis_tready (reducer__m_axis_tready      ) ,
    .m_axis_tvalid (reducer__m_axis_tvalid      ) ,
    .m_axis_tlast  (reducer__m_axis_tlast       ) 
  );

  assign reducer__iclk          = '0;
  assign reducer__irst          = '0;
  assign reducer__s_axis_tdata  = '0;
  assign reducer__s_axis_tvalid = '0;
  assign reducer__s_axis_tlast  = '0;
  assign reducer__m_axis_tready = '0;
*/ 

module axis_reducer
#(
  parameter int S_DATA_WIDTH = 8 ,
  parameter int M_DATA_WIDTH = 1
)
(
  input  logic                     iclk          ,
  input  logic                     irst          ,

  input  logic [S_DATA_WIDTH-1:0]  s_axis_tdata  ,
  input  logic                     s_axis_tvalid ,
  output logic                     s_axis_tready ,
  input  logic                     s_axis_tlast  ,

  output logic [M_DATA_WIDTH-1:0]  m_axis_tdata  ,
  output logic                     m_axis_tvalid ,
  input  logic                     m_axis_tready ,
  output logic                     m_axis_tlast
);

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  logic         [S_DATA_WIDTH-1:0] shft_reg = 0;
  logic [$clog2(S_DATA_WIDTH)-1:0] cnt      = 0;
  logic                            dat;
  logic                            rdy;

  enum logic [1:0] {
    ST_IDLE = 2'd0,
    ST_REQ = 2'd1,
    ST_RD = 2'd2
  } ST;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    if (irst)  ST <= ST_IDLE;
    else case (ST)
      ST_IDLE : begin
        if (s_axis_tvalid)  ST <= ST_REQ;
      end

      ST_REQ  : begin
        ST <= ST_RD;
      end

      ST_RD   : begin
        if (cnt == S_DATA_WIDTH-1 && m_axis_tvalid)  ST <= ST_IDLE;
      end

      default : ST <= ST_IDLE;
    endcase
  end

  always_ff@(posedge iclk) begin
    if (irst)                     cnt <= '0;
    else if (m_axis_tvalid) begin
      if (cnt == S_DATA_WIDTH-1)  cnt <= '0;
      else                        cnt <= cnt + 1'b1;
    end
  end

  always_ff@(posedge iclk) begin
    if (irst) begin
      shft_reg <= '0;
    end if (s_axis_tvalid && s_axis_tready) begin
      shft_reg <= s_axis_tdata;
    end else if (m_axis_tready) begin
      shft_reg <= shft_reg >> 1;
    end
  end

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  assign s_axis_tready = (ST == ST_REQ);

  // always_comb begin
  always_ff@(posedge iclk) begin
    if (m_axis_tready) begin
      m_axis_tdata <= shft_reg[0];
      m_axis_tvalid <= (ST != ST_IDLE);
    end else begin
      m_axis_tvalid <= 1'b0;
    end
  end

endmodule
