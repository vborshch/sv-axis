
//
// Decoder for 5b to 4b
// Used in 100BASE-X Ethernet (802.3)
//

module dec4b5b
  (
    input              iclk  ,
    input              irst  ,
    input              iena  ,
    input        [4:0] idat  ,
    output logic       oena  ,
    output logic [3:0] ospec , // from 1 to 9 - special symbols,
                               // 0 - data symbol,
                               // F - invalid symbol
    output logic [3:0] odat    // valid if \ospec == '0\ only
  );

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  localparam logic [4:0] pINV [7] = '{5'b00001, 5'b00010, 5'b00011, 5'b00101, 5'b01000, 5'b01100, 5'b10000};

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  logic [3:0] dat;
  logic [3:0] spec;
  logic       ena;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------
  
  always_ff@(posedge iclk) begin
    if (irst)  ena <= 1'b0;
    else       ena <= iena;
  end

  always_ff@(posedge iclk) begin
    dat[3] <= (!idat[3] & idat[1]) | (!idat[1] & idat[3] & idat[2]) | (!idat[2] & idat[4]);
    dat[2] <= (!idat[1] & idat[3] & idat[2]) | (!idat[2] & idat[3] & idat[1]) | (!idat[4] & idat[2]);
    dat[1] <= (!idat[1] & idat[2] & idat[4]) | (!idat[3] & idat[2] & idat[4]) | (!idat[4] & idat[2]);
    dat[0] <= idat[0];
  end

  // Detect special symbols
  always_ff@(posedge iclk) begin
    case (idat)
      default : spec <= 4'd0;
      5'b00100: spec <= 4'd1; // pH
      5'b11111: spec <= 4'd2; // pI
      5'b11000: spec <= 4'd3; // pJ
      5'b10001: spec <= 4'd4; // pK
      5'b00110: spec <= 4'd5; // pL
      5'b00000: spec <= 4'd6; // pQ
      5'b00111: spec <= 4'd7; // pR
      5'b11001: spec <= 4'd8; // pS
      5'b01101: spec <= 4'd9; // pT
      // Invalid symbols
      pINV[0] : spec <= 4'hF;
      pINV[1] : spec <= 4'hF;
      pINV[2] : spec <= 4'hF;
      pINV[3] : spec <= 4'hF;
      pINV[4] : spec <= 4'hF;
      pINV[5] : spec <= 4'hF;
      pINV[6] : spec <= 4'hF;
    endcase
  end
  
  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    ospec <= spec;
    odat  <= dat;
    oena  <= ena;
  end

endmodule
