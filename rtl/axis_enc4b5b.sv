
//
// Encoder for 4b to 5b
// Used in 100BASE-X Ethernet (802.3)
//

module enc4b5b
  (
    input              iclk  ,
    input              irst  ,
    input              iena  ,
    input        [3:0] ispec , // from 1 to 9 - special symbols. 0 - normal operation
    input        [3:0] idat  , // valid if \ispec == 0\ only
    output logic       oena  ,
    output logic [4:0] odat
  );

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  logic [4:0] dat;
  logic [3:0] spec;
  logic [4:0] spec_dat;
  logic       ena;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------
  
  always_ff@(posedge iclk) begin
    if (irst)   ena <= 1'b0;
    else        ena <= iena;
    spec <= ispec;
  end

  always_ff@(posedge iclk) begin
    if (iena) begin
      dat[4] <= idat[3] | (!idat[2] & idat[1]) | (!idat[2] & !idat[0]);
      dat[3] <= idat[2] | (!idat[3] & !idat[1]);
      dat[2] <= idat[1] | (!idat[3] & !idat[2] & !idat[0]);
      dat[1] <= (!idat[1] & !idat[0]) | (idat[3] ^ idat[2]) | (idat[2] & !idat[1]);
      dat[0] <= idat[0];
    end
  end

  // Append special symbols
  always_ff@(posedge iclk) begin
    case (ispec)
      4'd1 : spec_dat <= 5'b00100; // pH
      4'd2 : spec_dat <= 5'b11111; // pI
      4'd3 : spec_dat <= 5'b11000; // pJ
      4'd4 : spec_dat <= 5'b10001; // pK
      4'd5 : spec_dat <= 5'b00110; // pL
      4'd6 : spec_dat <= 5'b00000; // pQ
      4'd7 : spec_dat <= 5'b00111; // pR
      4'd8 : spec_dat <= 5'b11001; // pS
      4'd9 : spec_dat <= 5'b01101; // pT
    endcase
  end

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    if (irst)                  odat <= '0;
    else if (ena && (~|spec))  odat <= dat;
      else                     odat <= spec_dat;

    if (irst)  oena <= 1'b0;
    else       oena <= ena;
  end

endmodule
